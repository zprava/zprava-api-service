all: test lint

test:
	python manage.py test

lint:
	pycodestyle --exclude=.git,__pycache__,venv,migrations .

.PHONY: all test lint
