from django.apps import AppConfig


class ManyauthtokenConfig(AppConfig):
    name = 'manyauthtoken'
