# Zprava API Service

## Getting Started

Install Python 3 (ideally 3.6 or later) and build dependencies for `mysqlclient` (pip install mysqlclient).
NOTE: mysqlclient will not build on windows 10 with python 3.7, < 3.6.5 is needed.
Then run `pip install -r requirements.txt`.

To ensure consistent text formatting such as linefeed (LF) line ending endings, final newline,
and 4 space indents, make sure [editorconfig][editorconfig] is enabled for your editor.

[editorconfig]: https://editorconfig.org/

### Running a Database

One way to easily start up MariaDB for your particular application is to use Docker.
Alternately, one can also set up MariaDB as normal.

```sh
docker run --name zprava-mariadb \
    -e MYSQL_RANDOM_ROOT_PASSWORD=yes \
    -e MYSQL_DATABASE=zprava \
    -e MYSQL_USER=zuser \
    -e MYSQL_PASSWORD=zpass \
    -p 3306:3306 \
    -d mariadb:10 \
    # Include the following line to map a local directory into the Docker container
    -v /my/own/datadir:/var/lib/mysql
```

N.B. the `-v` line is optional, but without it, you won't save your database when the
docker container stops.

### Configuring Django to Run

You can configure the database that Django uses with environment variables or `.env` in this directory.
By default it will use defaults that will work with the above docker invocation/database setup.
Otherwise create an `.env` file or use environment variables. See [`dotenv.example`](./dotenv.example) for an example.

| Environment variable              | Its use                                                                                                                 |
| --------------------------------- | ----------------------------------------------------------------------------------------------------------------------- |
| `DJANGO_DB_NAME`                  | Name of the table                                                                                                       |
| `DJANGO_DB_USER`                  | mysql user                                                                                                              |
| `DJANGO_DB_PASSWORD`              | mysql password                                                                                                          |
| `DJANGO_DB_HOST`                  | hostname                                                                                                                |
| `DJANGO_DB_PORT`                  | port                                                                                                                    |
| `DJANGO_DB_ENGINE`                | *Optional.* In case another database engine is required.                                                                |
| `DJANGO_ALLOWED_HOSTS`            | *Optional.* List of hosts to permit access to the application.                                                          |
| `DJANGO_DEBUG`                    | *Optional.* Defaults to `true`. When true, run en DEBUG mode.                                                           |
| `DJANGO_SECRET_KEY`               | *Optional.* The Django secret key. Should stay the same between restarts.                                               |
| `DJANGO_LOG_LEVEL`                | *Optional.* Can be `DEBUG`, `INFO`, `WARNING`, `ERROR` or `CRITICAL`. Default is `INFO`.                                |
| `DJANGO_FCM_URL`                  | Necessary to use push notifications. Should be like `https://fcm.googleapis.com/v1/projects/lol123-1aa96/messages:send` |
| `DJANGO_FCM_SERVICE_ACCOUNT_PATH` | Necessary to use push notifications. Path to the service account json file.                                             |

### Running Everything in Docker

One can run the entire project (both Mariadb and Django) using [`docker-compose`][docker-compose].
[More information on how to use `docker-compose`](https://docs.docker.com/compose/gettingstarted/).

Simply run:

```sh
docker-compose up
```

Note, upon initial launch, MariaDB has to initialize its settings, so the Django application will
probably restart a few times. If it doesn't ever connect successfully, try stopping the `docker-compose`
images (e.g. `Control-C` the running `docker-compose up` or run `docker-compose stop`), then run
`docker-compose up` again.


[docker-compose]: https://docs.docker.com/compose/overview/

## Testing

To test run `python manage.py test`.

When writing tests, note that the response structure is not the same as what you get when
you use a web client. For example `{ "data": { "a_key": "a_value" }}` will be showen as
`{ "a_key": "a_value" }`. There are seperate tests to test the renderer (how the JSON is 
organized for the client - see `api/tests/test_renderer.py`. Refer to the following
table when writing tests:

| Response type    | What the client sees                                                            | What the tests see                                           |
| ---------------- | ------------------------------------------------------------------------------- | ------------------------------------------------------------ |
| Single object    | `{ "data": { ... }}`                                                            | `{ ... }`                                                    |
| Many objects     | `{ "data": { "items": [ ... ]}}`                                                | `[ ... ]`                                                    |
| Validation error | `{ "error": { "code": "400", "message": "Validation Error", "items": [ { "field": "text"}, ... ]}}` | Same except `"text"` is an 'ErrorDetail' |
| General errors   | `{ "error": { "code": "400", "message": ... }}`                                 | Same except `"message"` value is an `ErrorDetail`            |

## Project structure

The structure is a little different from the most common Django layout; more information
[here](https://www.revsys.com/tidbits/recommended-django-project-layout/).


```text
zprava-api-service            # the django project root
├── api                       # main API application
│   ├── admin.py
│   ├── apps.py
│   ├── __init__.py
│   ├── migrations
│   ├── models.py
│   ├── tests.py
│   └── views.py
├── db.sqlite3
├── manage.py
├── README.md
├── requirements.txt
└── zprava                    # the django project's "app" (settings, main router)
    ├── __init__.py
    ├── settings.py
    ├── urls.py
    └── wsgi.py

```

## TODO

1. Import schemas from database from database & tweak resulting models
(the rest may be out of order or interrelated)
2. Add Serializer instances for our models (dictates how to serialize the models to JSON)
3. Add Routes
4. Add Business Logic
5. Make authentication work
6. Tests
7. Integrate into mobile app
8. Test mobile app
9. Dockerize

