from rest_framework import viewsets
from api.serializers import ConversationSerializer
from api.permissions import IsAuthenticated


class ConversationViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated, )
    serializer_class = ConversationSerializer

    def get_queryset(self):
        return self.request.user.conversation_set.all()
