from django.contrib.auth import authenticate
from manyauthtoken.models import Token
from rest_framework import viewsets, status
from rest_framework.decorators import action, api_view, list_route
from rest_framework.exceptions import AuthenticationFailed
from api.permissions import AllowAny, IsAuthenticated, DenyAll, IsSameUser
from django.shortcuts import get_object_or_404
from rest_framework.response import Response
from api.models import User
from api.serializers import (UserSerializer,
                             LogInSerializer, TokenSerializer,
                             LogOutSerializer)
from .mixins import ActionPermissionMixin


class UserViewSet(ActionPermissionMixin, viewsets.ModelViewSet):

    # Via https://stackoverflow.com/a/35987077/2720026
    permission_classes_by_action = {
        'retrieve': (IsAuthenticated, ),
        'create': (AllowAny, ),
        'log_in': (AllowAny, ),
        'log_out': (IsAuthenticated, ),
        'update': (IsSameUser, ),
        'me': (IsAuthenticated, ),
    }

    permission_classes = (DenyAll, )
    queryset = User.objects.all()
    serializer_class = UserSerializer

    @list_route(methods=['POST'])
    def log_in(self, request):
        serializer = LogInSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = authenticate(
            username=serializer.validated_data['username'],
            password=serializer.validated_data['password'],
        )
        if user:
            token = Token(user=user)
            if 'device_id' in serializer.validated_data:
                token.device_id = serializer.validated_data['device_id']
            token.save()
            return Response(TokenSerializer(token).data)
        else:
            raise AuthenticationFailed('invalid credentials')

    def update(self, request, *args, **kwargs):
        serializer = self.serializer_class(request.user, data=request.data, partial=True)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_200_OK)

    @list_route(methods=['POST'])
    def log_out(self, request):
        token = request.auth
        token.delete()
        return Response(LogOutSerializer({'logged_out': True}).data)

    @list_route(methods=['GET', 'PUT'])
    def me(self, request):
        # See https://stackoverflow.com/questions/51149599/call-viewset-method-from-another-view
        if request.method == 'GET':
            view = UserViewSet.as_view({'get': 'retrieve'})
            return view(request._request, pk=request.user.id)
        elif request.method == 'PUT':
            view = UserViewSet.as_view({'put': 'update'})
            return view(request._request, pk=request.user.id)
        assert False, 'Should not reach'
