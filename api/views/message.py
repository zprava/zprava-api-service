from rest_framework import viewsets
from rest_framework_nested.viewsets import NestedViewSetMixin
from api.permissions import AllowAny, IsAuthenticated, DenyAll, CanReadMessage
from api.models import Message
from api.serializers import MessageSerializer
from .mixins import ActionPermissionMixin
from django.utils.dateparse import parse_datetime
from api.models import Message, Conversation


class MessageViewSet(ActionPermissionMixin, viewsets.ModelViewSet):

    permission_classes_by_action = {
        # 'retrieve': (IsAuthenticated, ),
        # 'list': (IsAuthenticated, ),
        # 'create': (IsAuthenticated, ),
    }

    permission_classes = (CanReadMessage, )
    serializer_class = MessageSerializer

    def get_queryset(self):
        qs = Conversation.objects.filter(id=self.kwargs['conversation_pk'],
                                         users__id=self.request.user.id)
        return Message.objects.filter(conversation__in=qs)
