from django.urls import path, include
from rest_framework.routers import DefaultRouter
from rest_framework_nested.routers import NestedSimpleRouter
from api import views

# Create a router and register our viewsets with it.
router = DefaultRouter(trailing_slash=False)
router.register(
    r'user',
    views.UserViewSet,
    base_name='user',
)
router.register(
    r'conversation',
    views.ConversationViewSet,
    base_name='conversation',
)

messages_router = NestedSimpleRouter(
    router,
    r'conversation',
    lookup='conversation',
)
messages_router.register(
    r'message',
    views.MessageViewSet,
    base_name='messages',
)

# The API URLs are now determined automatically by the router.
urlpatterns = [
    path('v1/', include(router.urls)),  # Version 1 of the API
    path('v1/', include(messages_router.urls)),
]
