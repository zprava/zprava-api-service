from rest_framework.test import APITestCase
from rest_framework import status


class TestAuthorization(APITestCase):
    """
    Make sure the Authorization header is required to use all routes except

    - /user/log_in POST
    - /user POST
    """

    ######################################################################
    # The following few tests should fail but not with 401 unauthorized,
    # as they are publically accessible.
    ######################################################################

    def test_user__post(self):
        """
        This request will fail, but with a 400 bad request instead of
        401 unauthorized
        """
        response = self.client.post('/api/v1/user')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertNotIn('data', response.data)
        self.assertIn('error', response.data)

    def test_user_log_in__post(self):
        """
        This request will fail, but with a 400 bad request instead of
        401 unauthorized
        """
        response = self.client.post('/api/v1/user/log_in')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertNotIn('data', response.data)
        self.assertIn('error', response.data)

    ######################################################################
    # The rest of the tests should fail with 401 unauthorized.
    ######################################################################

    def test_user_id__get(self):
        response = self.client.get('/api/v1/user/1')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertNotIn('data', response.data)
        self.assertIn('error', response.data)

    def test_user_id__put(self):
        response = self.client.put('/api/v1/user/1')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertNotIn('data', response.data)
        self.assertIn('error', response.data)

    def test_user_me__get(self):
        response = self.client.get('/api/v1/user/me')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertNotIn('data', response.data)
        self.assertIn('error', response.data)

    def test_user_me__put(self):
        response = self.client.put('/api/v1/user/me')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertNotIn('data', response.data)
        self.assertIn('error', response.data)

    def test_user_log_out__post(self):
        response = self.client.post('/api/v1/user/log_out')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertNotIn('data', response.data)
        self.assertIn('error', response.data)

    def test_conversation__post(self):
        response = self.client.post('/api/v1/conversation')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertNotIn('data', response.data)
        self.assertIn('error', response.data)

    def test_conversation__get(self):
        response = self.client.get('/api/v1/conversation')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertNotIn('data', response.data)
        self.assertIn('error', response.data)

    def test_conversation_id__put(self):
        response = self.client.put('/api/v1/conversation/5')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertNotIn('data', response.data)
        self.assertIn('error', response.data)

    def test_conversation_id__get(self):
        response = self.client.get('/api/v1/conversation/5')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertNotIn('data', response.data)
        self.assertIn('error', response.data)

    def test_conversation_id_message_mid__get(self):
        response = self.client.get('/api/v1/conversation/5/message/1')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertNotIn('data', response.data)
        self.assertIn('error', response.data)

    def test_conversation_id_message__get(self):
        response = self.client.get('/api/v1/conversation/5/message')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertNotIn('data', response.data)
        self.assertIn('error', response.data)

    def test_conversation_id_message__post(self):
        response = self.client.post('/api/v1/conversation/5/message')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertNotIn('data', response.data)
        self.assertIn('error', response.data)
