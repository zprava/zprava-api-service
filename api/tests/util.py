import rest_framework.test
import contextlib


class ZpravaAPITestCase(rest_framework.test.APITestCase):

    class ExceptionReference(object):
        def __init__(self, exn_instance=None):
            self.exception = exn_instance

    @contextlib.contextmanager
    def __assertNotRaisesManager(self, exn, message=None):
        r = ZpravaAPITestCase.ExceptionReference()
        try:
            yield r
        except exn as e:
            r.exception = e
            self.fail(message or f'{e.__class__.__name__} was raised')

    def assertNotRaises(self, exn, *args, message=None):
        """
        Assert that callable `fn` does not raise Exceptions of type `exn`.

        `exn`     -- The exception type
        `fn`      -- The callable (function or lambda) to invoke
        `message` -- The message `str` to display when the assertion fails
        returns   -- `None`

        Example:

        D = {'k': 'v'}
        assertNotRaises(KeyError, lambda: D['k']) -> None

        D = {}
        assertNotRaises(KeyError, lambda: D['k']) -> fails testcase

        Can also do:

        with assertNotRaises(KeyError):
            D = {}
            D['k'] -> fails testcase
        """
        if not args:
            return self.__assertNotRaisesManager(exn, message)
        else:
            fn = args[0]
            with self.__assertNotRaisesManager(exn, message):
                fn()

        def assertDictEqual(self, actual, expected, msg=None):
            for k, v_expected in expected.items():
                self.assertIn(k, actual, msg)
                v_actual = actual[k]
                is_iterable = isinstance(v_expected, collections.Iterable)
                if is_iterable and not isinstance(expected, str):
                    self.assertItemsEqual(v_actual, v_expected, msg)
                else:
                    self.assertEqual(v_actual, v_expected, msg)
            return True
