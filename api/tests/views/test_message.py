from rest_framework import status
from api.tests.util import ZpravaAPITestCase
from api.models import User, Conversation


class TestMessage(ZpravaAPITestCase):

    fixtures = ['user_conversation_messages']

    def test_message_id__get__is_sender(self):
        u = User.objects.get(username='UserA')
        self.client.force_authenticate(user=u)
        r = self.client.get('/api/v1/conversation/2/message/1')
        self.assertEqual(r.status_code, status.HTTP_200_OK)
        self.assertIn('created', r.data)
        self.assertIn('updated', r.data)
        self.assertIn('deleted', r.data)

        self.assertIn('sender', r.data)
        self.assertIn('conversation', r.data)
        self.assertIn('kind', r.data)
        self.assertIn('content', r.data)

        self.assertEqual(r.data['id'], 1)
        self.assertEqual(r.data['sender'], {'id': 1, 'username': 'UserA'})
        self.assertEqual(r.data['conversation'], 2)
        self.assertEqual(r.data['kind'], 'text/plain')
        self.assertEqual(r.data['content'], 'VXNlckEgdG8gQzI=')

    def test_message_id__get__is_participant(self):
        u = User.objects.get(username='UserB')
        self.client.force_authenticate(user=u)
        r = self.client.get('/api/v1/conversation/3/message/3')
        self.assertEqual(r.status_code, status.HTTP_200_OK)
        self.assertIn('created', r.data)
        self.assertIn('updated', r.data)
        self.assertIn('deleted', r.data)

        self.assertIn('sender', r.data)
        self.assertIn('conversation', r.data)
        self.assertIn('kind', r.data)
        self.assertIn('content', r.data)

        self.assertEqual(r.data['id'], 3)
        self.assertEqual(r.data['sender'], {'id': 1, 'username': 'UserA'})
        self.assertEqual(r.data['conversation'], 3)
        self.assertEqual(r.data['kind'], 'text/plain')
        self.assertEqual(r.data['content'], 'VXNlckEgdG8gQzM=')

    def test_message_id__get__not_participant(self):
        u = User.objects.get(username='UserC')
        self.client.force_authenticate(user=u)
        r = self.client.get('/api/v1/conversation/3/message/3')
        self.assertEqual(r.status_code, status.HTTP_403_FORBIDDEN)
        self.assertIn('error', r.data)
        self.assertIn('message', r.data['error'])
        self.assertIn('code', r.data['error'])

    def test_message__get(self):
        u = User.objects.get(username='UserB')
        self.client.force_authenticate(user=u)
        r = self.client.get('/api/v1/conversation/3/message')
        self.assertEqual(r.status_code, status.HTTP_200_OK)
        self.assertEqual(len(r.data), 1)

        msgs = sorted(r.data, key=lambda c: c['id'])
        for i in msgs:
            self.assertIn('created', i)
            self.assertIn('updated', i)
            self.assertIn('deleted', i)
            self.assertIn('id', i)
            self.assertIn('sender', i)
            self.assertIn('conversation', i)
            self.assertIn('kind', i)
            self.assertIn('content', i)

        self.assertEqual(msgs[0]['id'], 3)
        self.assertEqual(msgs[0]['sender'], {'id': 1, 'username': 'UserA'})
        self.assertEqual(msgs[0]['conversation'], 3)
        self.assertEqual(msgs[0]['kind'], 'text/plain')
        self.assertEqual(msgs[0]['content'], 'VXNlckEgdG8gQzM=')

    def test_message__post(self):
        ub = User.objects.get(username='UserB')
        self.client.force_authenticate(user=ub)
        r = self.client.post('/api/v1/conversation/3/message', {
            'sender': {'id': ub.id},
            'conversation': 3,
            'kind': 'text/plain',
            'content': 'VXNlckIgdG8gQzM=',
        })
        self.assertEqual(r.status_code, status.HTTP_201_CREATED)
        self.assertIn('created', r.data)
        self.assertIn('updated', r.data)
        self.assertIn('deleted', r.data)

        self.assertIn('sender', r.data)
        self.assertIn('conversation', r.data)
        self.assertIn('kind', r.data)
        self.assertIn('content', r.data)

        self.assertEqual(r.data['sender'], {'id': 2, 'username': 'UserB'})
        self.assertEqual(r.data['conversation'], 3)
        self.assertEqual(r.data['kind'], 'text/plain')
        self.assertEqual(r.data['content'], 'VXNlckIgdG8gQzM=')
