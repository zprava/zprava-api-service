from rest_framework import status
from api.tests.util import ZpravaAPITestCase
from api.models import User, Conversation


class TestConversation(ZpravaAPITestCase):

    fixtures = ['user_conversations']

    def test_conversation_id__get(self):
        u = User.objects.get(username='UserA')
        self.client.force_authenticate(user=u)
        r = self.client.get('/api/v1/conversation/2')
        self.assertEqual(r.status_code, status.HTTP_200_OK)
        self.assertIn('created', r.data)
        self.assertIn('updated', r.data)
        self.assertIn('deleted', r.data)
        self.assertIn('last_active', r.data)
        self.assertIn('id', r.data)
        self.assertIn('users', r.data)
        self.assertIn('name', r.data)

        self.assertEqual(r.data['id'], 2)
        self.assertEqual(r.data['users'], [{'username': 'UserA', 'id': 1}])

    def test_conversation__get__not_participant(self):
        u = User.objects.get(username='UserA')
        self.client.force_authenticate(user=u)
        r = self.client.get('/api/v1/conversation/1')
        self.assertEqual(r.status_code, status.HTTP_404_NOT_FOUND)
        self.assertIn('error', r.data)
        self.assertIn('code', r.data['error'])
        self.assertIn('message', r.data['error'])

    def test_conversation_id__put(self):
        ua = User.objects.get(username='UserA')
        ub = User.objects.get(username='UserB')
        self.client.force_authenticate(user=ua)
        r = self.client.put('/api/v1/conversation/2', {
            'name': 'A new name',
            'users': [{'username': ua.username, 'id': ua.id},
                      {'username': ub.username, 'id': ub.id}],
        })
        self.assertEqual(r.status_code, status.HTTP_200_OK, r.data)
        self.assertIn('created', r.data)
        self.assertIn('updated', r.data)
        self.assertIn('deleted', r.data)
        self.assertIn('last_active', r.data)
        self.assertIn('id', r.data)
        self.assertIn('users', r.data)
        self.assertIn('name', r.data)

        self.assertEqual(r.data['id'], 2)
        self.assertEqual(r.data['users'],
                         [{'username': ua.username, 'id': ua.id},
                          {'username': ub.username, 'id': ub.id}])

    def test_conversation_id__put__user_representation(self):
        ua = User.objects.get(username='UserA')
        ub = User.objects.get(username='UserB')
        self.client.force_authenticate(user=ua)
        r = self.client.put('/api/v1/conversation/2', {
            'name': 'A new name',
            'users': [{'id': ua.id}, {'username': ub.username}],
        })
        self.assertEqual(r.status_code, status.HTTP_200_OK, r.data)
        self.assertIn('created', r.data)
        self.assertIn('updated', r.data)
        self.assertIn('deleted', r.data)
        self.assertIn('last_active', r.data)
        self.assertIn('id', r.data)
        self.assertIn('users', r.data)
        self.assertIn('name', r.data)

        self.assertEqual(r.data['id'], 2)
        self.assertEqual(r.data['users'],
                         [{'username': ua.username, 'id': ua.id},
                          {'username': ub.username, 'id': ub.id}])

    def test_conversation__get(self):
        u = User.objects.get(username='UserB')
        self.client.force_authenticate(user=u)
        r = self.client.get('/api/v1/conversation')
        self.assertEqual(r.status_code, status.HTTP_200_OK)
        self.assertEqual(len(r.data), 2)

        convs = sorted(r.data, key=lambda c: c['id'])
        for i in convs:
            self.assertIn('created', i)
            self.assertIn('updated', i)
            self.assertIn('deleted', i)
            self.assertIn('last_active', i)
            self.assertIn('id', i)
            self.assertIn('users', i)
            self.assertIn('name', i)

        self.assertEqual(convs[0]['id'], 3)
        self.assertEqual(convs[0]['users'], [{'username': 'UserA', 'id': 1},
                                             {'username': 'UserB', 'id': 2}])
        self.assertEqual(convs[1]['id'], 4)
        self.assertEqual(convs[1]['users'], [{'username': 'UserA', 'id': 1},
                                             {'username': 'UserB', 'id': 2},
                                             {'username': 'UserC', 'id': 3}])

    def test_conversation__post(self):
        ua = User.objects.get(username='UserA')
        ub = User.objects.get(username='UserB')
        self.client.force_authenticate(user=ua)
        r = self.client.post('/api/v1/conversation', {
            'name': 'A new conversation',
            'users': [{'username': ua.username, 'id': ua.id},
                      {'username': ub.username, 'id': ub.id}],
        })
        self.assertEqual(r.status_code, status.HTTP_201_CREATED, r.data)
        self.assertIn('created', r.data)
        self.assertIn('updated', r.data)
        self.assertIn('deleted', r.data)
        self.assertIn('last_active', r.data)
        self.assertIn('id', r.data)
        self.assertIn('users', r.data)
        self.assertIn('name', r.data)

        self.assertEqual(r.data['name'], 'A new conversation')
        self.assertEqual(r.data['users'], [{'username': 'UserA', 'id': 1},
                                           {'username': 'UserB', 'id': 2}])

    def test_conversation__post__user_representation(self):
        ua = User.objects.get(username='UserA')
        ub = User.objects.get(username='UserB')
        self.client.force_authenticate(user=ua)
        r = self.client.post('/api/v1/conversation', {
            'name': 'A new conversation',
            'users': [{'id': ua.id}, {'username': ub.username}],
        })
        self.assertEqual(r.status_code, status.HTTP_201_CREATED, r.data)
        self.assertIn('created', r.data)
        self.assertIn('updated', r.data)
        self.assertIn('deleted', r.data)
        self.assertIn('last_active', r.data)
        self.assertIn('id', r.data)
        self.assertIn('users', r.data)
        self.assertIn('name', r.data)

        self.assertEqual(r.data['name'], 'A new conversation')
        self.assertEqual(r.data['users'], [{'username': 'UserA', 'id': 1},
                                           {'username': 'UserB', 'id': 2}])

    def test_conversation__post__bad_user(self):
        ua = User.objects.get(username='UserA')
        self.client.force_authenticate(user=ua)
        r = self.client.post('/api/v1/conversation', {
            'name': 'test conversation',
            'users': [
                {'id': 500},
                {'username': 'invaliduser'},
                {'id': 3, 'username': 'UserC'},
            ],
        })
        self.assertEqual(r.status_code, status.HTTP_400_BAD_REQUEST, r.data)
