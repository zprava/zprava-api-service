from rest_framework.test import APITestCase
from rest_framework import status
from api.models import User
from manyauthtoken.models import Token
from api.tests.util import ZpravaAPITestCase


class TestRegisterAndLogIn(ZpravaAPITestCase):

    fixtures = ['users']  # See api/fixtures/users.json

    username = 'billy'
    email = 'billy@example.com'
    phone = '55555555555'
    password = 'averylongpassword1234566$$$'
    first_name = 'Billy'
    last_name = 'Bob'

    def test_register(self):
        response = self.client.post('/api/v1/user', {
            'username': self.username,
            'email': self.email,
            'phone': self.phone,
            'password': self.password,
            'first_name': self.first_name,
            'last_name': self.last_name,
        })
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        with self.assertNotRaises(User.DoesNotExist):
            u = User.objects.get(username=self.username)
        self.assertEqual(u.username, self.username)
        self.assertNotEqual(u.password, self.password,
                            'Password should be hashed, not plaintext')
        self.assertEqual(u.phone, self.phone)
        self.assertEqual(u.first_name, self.first_name)
        self.assertEqual(u.last_name, self.last_name)

        d = response.data
        self.assertIsNone(d['deleted'])
        self.assertIsNotNone(d['created'])
        self.assertIsNotNone(d['updated'])

        self.assertEqual(d['username'], self.username)
        self.assertFalse('password' in d)
        self.assertEqual(d['first_name'], self.first_name)
        self.assertEqual(d['last_name'], self.last_name)
        self.assertEqual(d['phone'], self.phone)

    def test_log_in(self):
        response = self.client.post('/api/v1/user/log_in', {
            'username': 'UserA',
            'password': 'PasswordA',
        })
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertIn('token', response.data)

    def test_double_log_in(self):
        response = self.client.post('/api/v1/user/log_in', {
            'username': 'UserA',
            'password': 'PasswordA',
        })
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertIn('token', response.data)
        t1 = response.data['token']
        response2 = self.client.post('/api/v1/user/log_in', {
            'username': 'UserA',
            'password': 'PasswordA',
        })
        self.assertEqual(response2.status_code, status.HTTP_200_OK)
        self.assertIn('token', response2.data)
        t2 = response2.data['token']
        with self.assertNotRaises(Token.DoesNotExist):
            Token.objects.get(key=t1)
        with self.assertNotRaises(Token.DoesNotExist):
            Token.objects.get(key=t2)

    def test_log_out(self):
        u = User.objects.get(username='UserB')
        t = Token.objects.create(user=u)
        t.save()
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + str(t))
        r = self.client.post('/api/v1/user/log_out')
        self.assertEqual(r.status_code, status.HTTP_200_OK)
        self.assertIn('logged_out', r.data)
        self.assertTrue(r.data['logged_out'])
        self.assertRaises(Token.DoesNotExist, t.refresh_from_db)

    def test_user__get(self):
        u = User.objects.get(username='UserB')
        uid = u.id
        self.client.force_authenticate(user=u)
        r = self.client.get(f'/api/v1/user/{uid}')
        self.assertEqual(r.status_code, status.HTTP_200_OK)
        self.assertEqual(r.data['id'], uid)
        self.assertEqual(r.data['username'], 'UserB')
        self.assertEqual(r.data['email'], 'UserB@example.com')
        self.assertEqual(r.data['first_name'], 'FirstB')
        self.assertEqual(r.data['last_name'], 'LastB')
        self.assertEqual(r.data['phone'], '5555556666')

    # XXX: Test optional password field
    def test_user__put(self):
        u = User.objects.get(username='UserC')
        uid = u.id
        self.client.force_authenticate(user=u)
        r = self.client.put('/api/v1/user/{}'.format(uid), {
            'id': uid,
            'username': 'UserC',
            'email': 'UserC@example.com',
            'first_name': 'changed1',
            'last_name': 'changed2',
            'phone': '5555550000',
            'password': 'changeme',
        })
        self.assertEqual(r.status_code, status.HTTP_200_OK, str(r.data))
        self.assertEqual(r.data['id'], uid)
        self.assertEqual(r.data['username'], 'UserC')
        self.assertEqual(r.data['email'], 'UserC@example.com')
        self.assertEqual(r.data['first_name'], 'changed1')
        self.assertEqual(r.data['last_name'], 'changed2')
        self.assertEqual(r.data['phone'], '5555550000')
        u.refresh_from_db()
        self.assertEqual(u.id, uid)
        self.assertEqual(u.username, 'UserC')
        self.assertEqual(u.email, 'UserC@example.com')
        self.assertEqual(u.first_name, 'changed1')
        self.assertEqual(u.last_name, 'changed2')
        self.assertEqual(u.phone, '5555550000')

    def test_user_me__get(self):
        u = User.objects.get(username='UserB')
        uid = u.id
        self.client.force_authenticate(user=u)
        r = self.client.get('/api/v1/user/me')
        self.assertEqual(r.status_code, status.HTTP_200_OK)
        self.assertEqual(r.data['id'], uid)
        self.assertEqual(r.data['username'], 'UserB')
        self.assertEqual(r.data['email'], 'UserB@example.com')
        self.assertEqual(r.data['first_name'], 'FirstB')
        self.assertEqual(r.data['last_name'], 'LastB')
        self.assertEqual(r.data['phone'], '5555556666')

    def test_user_me__put(self):
        u = User.objects.get(username='UserC')
        uid = u.id
        self.client.force_authenticate(user=u)
        r = self.client.put('/api/v1/user/me', {
            'id': uid,
            'username': 'UserC',
            'email': 'UserC@example.com',
            'first_name': 'changed11',
            'last_name': 'changed22',
            'phone': '5555550001',
            'password': 'changeme1',
        })
        self.assertEqual(r.status_code, status.HTTP_200_OK, str(r.data))
        self.assertEqual(r.data['id'], uid)
        self.assertEqual(r.data['username'], 'UserC')
        self.assertEqual(r.data['email'], 'UserC@example.com')
        self.assertEqual(r.data['first_name'], 'changed11')
        self.assertEqual(r.data['last_name'], 'changed22')
        self.assertEqual(r.data['phone'], '5555550001')
        self.assertNotIn('password', r.data)
        u.refresh_from_db()
        self.assertEqual(u.id, uid)
        self.assertEqual(u.username, 'UserC')
        self.assertEqual(u.email, 'UserC@example.com')
        self.assertEqual(u.first_name, 'changed11')
        self.assertEqual(u.last_name, 'changed22')
        self.assertEqual(u.phone, '5555550001')

    def test_user_can_change_password(self):
        u = User.objects.get(username='UserC')
        self.client.force_authenticate(user=u)
        r = self.client.put('/api/v1/user/me', {
            'id': u.id,
            'username': u.username,
            'email': u.email,
            'first_name': u.first_name,
            'last_name': u.last_name,
            'phone': u.phone,
            'password': 'topsecret',
        })
        self.assertEqual(r.status_code, status.HTTP_200_OK, str(r.data))
        self.client.force_authenticate(None)
        r = self.client.post('/api/v1/user/log_in', {
            'username': 'UserC',
            'password': 'topsecret',
        })
        self.assertEqual(r.status_code, status.HTTP_200_OK, str(r.data))
        self.assertIn('token', r.data)

    def test_user__put_no_password(self):
        u = User.objects.get(username='UserC')
        self.client.force_authenticate(user=u)
        r = self.client.put('/api/v1/user/me', {
            'id': u.id,
            'username': 'UserC',
            'email': 'UserC@example.com',
            'first_name': 'changed11',
            'last_name': 'changed22',
            'phone': '5555550001',
        })
        self.assertEqual(r.status_code, status.HTTP_200_OK, str(r.data))
        self.assertEqual(r.data['id'], u.id)
        self.assertEqual(r.data['username'], 'UserC')
        self.assertEqual(r.data['email'], 'UserC@example.com')
        self.assertEqual(r.data['first_name'], 'changed11')
        self.assertEqual(r.data['last_name'], 'changed22')
        self.assertEqual(r.data['phone'], '5555550001')
        self.assertNotIn('password', r.data)
        u.refresh_from_db()
        self.assertEqual(u.id, u.id)
        self.assertEqual(u.username, 'UserC')
        self.assertEqual(u.email, 'UserC@example.com')
        self.assertEqual(u.first_name, 'changed11')
        self.assertEqual(u.last_name, 'changed22')
        self.assertEqual(u.phone, '5555550001')
        self.client.force_authenticate(None)
        r = self.client.post('/api/v1/user/log_in', {
            'username': 'UserC',
            'password': 'PasswordC',
        })
        self.assertEqual(r.status_code, status.HTTP_200_OK, str(r.data))
        self.assertIn('token', r.data)
