from rest_framework.test import APITestCase
from rest_framework import status
from api.models import User, Message, Conversation
from manyauthtoken.models import Token
from api.tests.util import ZpravaAPITestCase


class TestPushNotifications(ZpravaAPITestCase):

    fixtures = ['user_conversation_messages']

    def test_login_with_device_id(self):
        DEVICE_ID = 'A TOP SECRET DEVICE ID'
        u = User.objects.get(username='UserA')
        r = self.client.post('/api/v1/user/log_in', {
            'username': 'UserA',
            'password': 'PasswordA',
            'device_id': DEVICE_ID,
        })
        self.assertEqual(r.status_code, status.HTTP_200_OK, str(r))
        t = Token.objects.get(key=r.data['token'])
        self.assertEqual(t.device_id, DEVICE_ID)
