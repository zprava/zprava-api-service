from rest_framework.test import APITestCase
from rest_framework import status
from api.models import User, Conversation
import json


class TestRenderer(APITestCase):
    """Tests to ensure the renderer works"""

    def test_single(self):
        response = self.client.post('/api/v1/user', {
            'username': 'billy',
            'password': 'nopenopenope',
            'email': 'billy@example.com',
            'phone': '55555555555',
            'first_name': 'Billy',
            'last_name': 'Bob',
        })
        response.render()
        d = json.loads(response.content)
        self.assertTrue('data' in d)
        data = d['data']
        self.assertEqual(data['username'], 'billy')

    def test_multiple(self):
        u = User.objects.create_user(
            username='billy2',
            password='example123',
            email='billy@example.com',
            phone='1234567890',
            first_name='Billy2',
            last_name='Bob2',
        )
        u.save()
        self.client.force_authenticate(user=u)
        response = self.client.get('/api/v1/conversation')
        response.render()
        d = json.loads(response.content)
        self.assertTrue('data' in d)
        self.assertTrue('items' in d['data'])
        self.assertTrue(isinstance(d['data']['items'], list))

    def test_general_error(self):
        """Should generate a 401 error"""
        response = self.client.get('/api/v1/user/5')
        response.render()
        d = json.loads(response.content)
        self.assertTrue('error' in d)
        e = d['error']
        self.assertTrue('code' in e)
        self.assertEqual(e['code'], str(status.HTTP_401_UNAUTHORIZED))
        self.assertTrue('message' in e)

    def test_validation_error(self):
        response = self.client.post('/api/v1/user')
        response.render()
        d = json.loads(response.content)
        self.assertTrue('error' in d)
        e = d['error']
        self.assertTrue('code' in e)
        self.assertEqual(e['code'], str(status.HTTP_400_BAD_REQUEST))
        self.assertTrue('message' in e)
        self.assertTrue('items' in e)
        self.assertTrue(len(e['items']) > 0)
