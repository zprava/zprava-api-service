"""
Signals for the api project.

N.B. some logging is done in the DEBUG level.
See README.md for how to set the log level to DEBUG using `.env'.
"""
from django.conf import settings
from django.db.models.signals import post_save, m2m_changed
from django.dispatch import receiver
from api.models import Message, Conversation, User
from manyauthtoken.models import Token
from api.serializers import (MessageSerializer, ConversationSerializer)
from oauth2client.service_account import ServiceAccountCredentials
import requests, json
import logging


logger = logging.getLogger('django')
access_token_info = None


def _get_access_token():
    """Retrieve a valid access token that can be used to authorize requests.
    :return: Access token.
    """
    global access_token_info
    if access_token_info is None:
        credentials = ServiceAccountCredentials.from_json_keyfile_name(
            settings.FCM['SERVICE_ACCOUNT_PATH'],
            settings.FCM['SCOPES'],
        )
        access_token_info = credentials.get_access_token()
    return access_token_info.access_token


def send_to_token(id, data, name):
    if None in (settings.FCM['URL'], settings.FCM['SERVICE_ACCOUNT_PATH']):
        logger.warning('Firebase is not configured. Push notifications are disabled.')
        return
    headers = {
        'Authorization': 'Bearer {}'.format(_get_access_token()),
        'Content-Type': 'application/json; UTF-8',
    }
    fcm_message = {
        'message': {
            'name': name,
            'token': id,
            'data': data,
            'notification': {
                'title': 'New {}'.format(name),
                'body': 'You have a new {} waiting for you'.format(name),
            },
        },
    }
    logger.debug('Sending push notification with headers:{} fcm_message:{}'.format(headers, fcm_message))
    resp = requests.post(
        settings.FCM['URL'],
        data=json.dumps(fcm_message),
        headers=headers,
    )
    logger.debug('Response from FCM service: {}'.format(resp))


@receiver(post_save, sender=Message)
def message_push(sender, instance, **kwargs):
    L = list(filter(lambda x: x.id != instance.sender.id, instance.conversation.users.all()))
    for u in L:
        for t in u.auth_tokens.all():
            payload = {
                'notificationType': 'msg',
                'sender': instance.sender.username,
                'conversationID': str(instance.conversation.id),
                'conversationName': instance.conversation.name,
            }
            send_to_token(
                t.device_id,
                payload,
                'Message',
            )


@receiver(m2m_changed, sender=Conversation.users.through)
def conversation_push(sender, instance, **kwargs):
    for u in instance.users.all():
        for t in u.auth_tokens.all():
            payload = {
                'notificationType': 'con',
                'conversation': instance.name,
            }
            send_to_token(
                t.device_id,
                payload,
                'Conversation',
            )
