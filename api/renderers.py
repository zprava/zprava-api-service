from rest_framework import renderers


class CustomJSONRenderer(renderers.JSONRenderer):

    format = 'custom-json'

    def render(self, data, accepted_media_type=None, renderer_context=None):
        response_data = {}
        if data is not None:
            if isinstance(data, list):
                response_data['data'] = {'items': data}
            elif 'error' in data:
                response_data = data
            else:
                response_data['data'] = data
        return super(CustomJSONRenderer, self).render(
            response_data,
            accepted_media_type,
            renderer_context,
        )
