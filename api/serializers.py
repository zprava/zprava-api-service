# FIXME: Inherit from one Serializer that includes the BASE_FIELDS?
from rest_framework import serializers, relations
from api.models import User, Conversation, AbstractModel, Message


BASE_FIELDS = ('id', 'created', 'updated', 'deleted',)
BASE_READ_ONLY_FIELDS = BASE_FIELDS


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('username', 'email', 'phone', 'password',
                  'first_name', 'last_name', ) + BASE_FIELDS
        read_only_fields = BASE_READ_ONLY_FIELDS
        extra_kwargs = {'password': {'write_only': True, 'required': False}}

    def update(self, instance, validated_data):
        password = validated_data.pop('password', None)
        for key, value in validated_data.items():
            setattr(instance, key, value)
        if password is not None:
            instance.set_password(password)
        instance.save()
        return instance

    def create(self, validated_data):
        user = User(
            email=validated_data['email'],
            username=validated_data['username'],
            phone=validated_data.get('phone'),
            first_name=validated_data['first_name'],
            last_name=validated_data['last_name'],
        )
        user.set_password(validated_data['password'])
        user.save()
        return user


class LogInSerializer(serializers.Serializer):
    username = serializers.CharField()
    password = serializers.CharField()
    device_id = serializers.CharField(required=False)


class TokenSerializer(serializers.Serializer):
    token = serializers.CharField(source='key')


class LogOutSerializer(serializers.Serializer):
    logged_out = serializers.BooleanField()


class TerseUserField(relations.RelatedField):
    """
    Custom related representation of a User such that:

    {'username': 'theusername'}
    or
    {'id': 1}
    or both key/values represent a user.

    The username key's value is matched in an case-insensitive fashion.
    """
    queryset = User.objects.all()

    def to_representation(self, value):
        return {'username': value.username, 'id': value.id}

    def to_internal_value(self, data):
        if not isinstance(data, dict):
            raise serializers.ValidationError('User should be an object')
        kwargs = {}
        if 'id' in data:
            kwargs['id'] = data['id']
        if 'username' in data:
            kwargs['username__iexact'] = data['username']
        if not kwargs:
            raise serializers.ValidationError('id or username is required')
        try:
            return User.objects.get(**kwargs)
        except User.DoesNotExist:
            L = ['{}:{}'.format(key, data[key]) for key in ['id', 'username'] if key in data]
            raise serializers.ValidationError('user with {}'.format(' and '.join(L)))


class ConversationSerializer(serializers.ModelSerializer):
    users = TerseUserField(many=True)

    class Meta:
        model = Conversation
        fields = ('users', 'name', 'last_active',) + BASE_FIELDS
        read_only_fields = ('last_active',) + BASE_READ_ONLY_FIELDS


class MessageSerializer(serializers.ModelSerializer):
    sender = TerseUserField()

    class Meta:
        model = Message
        fields = ('conversation', 'sender', 'kind', 'content', ) + BASE_FIELDS
        read_only_fields = BASE_READ_ONLY_FIELDS

    def create(self, data):
        message = Message(
            conversation=data['conversation'],
            sender=data['sender'],
            kind=data['kind'],
            content=data['content'],
        )
        message.save()
        return message
