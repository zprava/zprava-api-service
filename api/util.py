from rest_framework.views import exception_handler


def custom_exception_handler(exc, context):
    response = exception_handler(exc, context)

    if response is not None:
        if isinstance(response.data, dict):
            message = response.data.get('detail')
        else:
            message = None
        error = {'code': str(response.status_code)}
        if not message:
            message = 'Valditation Error'
            error['items'] = response.data
        error['message'] = message
        response.data = {'error': error}
    return response
