from django.db import models
from django.utils import timezone
from django.contrib.auth.models import AbstractUser


# Make sure to add models to __all__ !
# https://stackoverflow.com/questions/44834/can-someone-explain-all-in-python
#
# Documentation:
# https://docs.python.org/3/reference/simple_stmts.html?highlight=__all__#the-import-statement
__all__ = ['User', 'Conversation', 'Message']


class AbstractModel(models.Model):
    """Abstract model for all API-accessible resources to inherit from

    All models should subclass this.

    Originally inspired by https://stackoverflow.com/a/1737078/2720026
    """
    created = models.DateTimeField(editable=False)
    updated = models.DateTimeField()
    deleted = models.DateTimeField(null=True)

    def save(self, *args, **kwargs):
        """On save, update timestamps."""
        if not self.id:
            self.created = timezone.now()
        self.updated = timezone.now()
        return super(AbstractModel, self).save(*args, **kwargs)

    class Meta:
        abstract = True


class User(AbstractUser, AbstractModel):
    """The main User model"""
    phone = models.CharField(max_length=30, blank=True, null=True)


class Conversation(AbstractModel):
    """A conversation that contains users & messages"""
    users = models.ManyToManyField(User)
    name = models.CharField(max_length=20)

    @property
    def last_active(self):
        """Timestamp of the last updated/deleted/created object.

        This can be a field on a message or the conversation itself
        """
        messages = self.message_set.order_by('-deleted', '-updated')
        t = max_timestamps(self)
        if not messages.exists():
            return t
        return max(t, max_timestamps(messages.first()))


class Message(AbstractModel):
    """A message

    `conversation` -- The Conversation the Message belongs to
    `sender`       -- The User who sent the Message
    `kind`         -- MIME type of the message (text/plain for plain text)
    `content`      -- Base64 encoded payload of the message
    """
    conversation = models.ForeignKey(Conversation, on_delete=models.CASCADE)
    sender = models.ForeignKey(User, on_delete=models.CASCADE)
    kind = models.CharField(max_length=100)
    content = models.TextField()


def max_timestamps(obj):
    t = max(obj.updated, obj.created)
    if obj.deleted:
        return max(t, obj.deleted)
    return t
