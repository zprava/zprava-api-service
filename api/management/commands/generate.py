from django.core.management.base import BaseCommand, CommandError
from api.models import User, Conversation, Message
import base64


class Command(BaseCommand):
    help = 'Generate dummy data'

    def add_arguments(self, parser):
        parser.add_argument('n_users', nargs=1, type=int)
        parser.add_argument('n_conversations', nargs=1, type=int)
        parser.add_argument('n_messages', nargs=1, type=int)

    def handle(self, *args, **options):
        n_users = options['n_users'][0]
        n_conversations = options['n_conversations'][0]
        n_messages = options['n_messages'][0]
        print('Creating users...')
        for i in range(n_users):
            u = User.objects.create(
                username=f'User{i}',
                email=f'User{i}@example.com',
                first_name=f'First{i}',
                last_name=f'Last{i}',
                phone=f'555{i:07}',
            )
            u.set_password(f'Password{i}')
            u.save()
        print('Creating conversations...')
        for i in range(n_conversations):
            c = Conversation.objects.create(
                name=f'Conversation {i}',
            )
            for user in User.objects.filter(id__lte=i):
                c.users.add(user)
            c.save()
        print('Creating messages...')
        for i in range(1, n_messages+1):
            conversation_id = max(i // n_conversations, 2)
            conversation = Conversation.objects.get(id=conversation_id)
            b64_bytes = base64.b64encode(f'Hello world! ({i})'.encode('ascii'))
            m = Message.objects.create(
                conversation=conversation,
                sender=conversation.users.order_by('?')[0],
                kind='text/plain',
                content=b64_bytes.decode('ascii'),
            )
            m.save()
