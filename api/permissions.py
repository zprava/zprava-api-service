from rest_framework.permissions import (AllowAny, IsAuthenticated,
                                        BasePermission)
from api.models import Conversation


class DenyAll(BasePermission):
    message = 'Not allowed at all'

    def has_permission(self, request, view):
        return False


class IsSameUser(BasePermission):
    """Use on views with the queryset of User.objects"""
    message = 'Must be same user'

    def has_permission(self, request, view):
        return (IsAuthenticated().has_permission(request, view)
                and int(view.kwargs['pk']) == request.user.id)


class IsConversationParticipant(BasePermission):
    """Use on views with the queryset of Conversation.objects"""
    message = 'Must be conversation participant'

    def has_permission(self, request, view):
        raise NotImplemented()
        if not IsAuthenticated().has_permission(request, view):
            return False
        qs = Conversation.objects.filter(users__id=request.user.id,
                                         id=int(view.kwargs['pk']))
        return qs.exists()


class CanReadMessage(BasePermission):
    """Use on MessageViewSet"""

    message = 'Must be conversation participant'

    def has_permission(self, request, view):
        if not IsAuthenticated().has_permission(request, view):
            return False
        qs = Conversation.objects.filter(users=request.user.id,
                                         id=view.kwargs['conversation_pk'])
        return qs.exists()
