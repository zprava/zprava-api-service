FROM python:3.6.7-alpine

ENV PYTHONUNBUFFERED 1
WORKDIR /code

COPY requirements.txt ./
RUN apk add build-base mariadb-dev
RUN pip install --no-cache-dir -r requirements.txt

COPY . /code/

CMD ["./docker-entrypoint.sh"]
